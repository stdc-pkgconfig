sinclude(m4/acx_pthread.m4)
AC_ARG_ENABLE(
    [pthread],
    AC_HELP_STRING([--disable-pthread], [Disable libpthread.pc generation]),
    [enable_pthread="${enableval}"],
    [enable_pthread=yes]
)

if test "${enable_pthread}" == "yes" ; then
    ACX_PTHREAD([have_pthread=yes])
    if test "$have_pthread" != "yes" ; then
	AC_MSG_ERROR([pthread NOT FOUND])
    fi
    AC_SUBST(PTHREAD_LIBS)
    AC_SUBST(PTHREAD_CFLAGS)
    echo ""
    echo "pthread: have_pthread=$have_pthread"
    echo "         PTHREAD_CFLAGS=$PTHREAD_CFLAGS"
    echo "         PTHREAD_LIBS=$PTHREAD_LIBS"
    echo ""
    AC_OUTPUT(libc-pthread.pc)
else
    echo "pthread disabled"
fi

AM_CONDITIONAL(ENABLE_PTHREAD, test "$enable_pthread" = "yes")
