AC_ARG_ENABLE(
    [math],
    AC_HELP_STRING([--disable-math],[Disable libm.pc generation]),
    [enable_math="${enableval}"],
    [enable_math=yes]
)

if test "${enable_math}" == "yes" ; then
    AC_CHECK_LIB([m],[sqrt],,AC_MSG_ERROR([missing libm]))
    AC_CHECK_HEADER([math.h],,AC_MSG_ERROR([missing math.h]))
    MATH_LIBS=-lm
    AC_SUBST(MATH_LIBS)
    AC_SUBST(MATH_CFLAGS)
    echo ""
    echo "math:   MATH_CFLAGS=$MATH_CFLAGS"
    echo "        MATH_LIBS=$MATH_LIBS"
    echo ""
    AC_OUTPUT(libc-math.pc)
else
    echo "math disabled"
fi

AM_CONDITIONAL(ENABLE_MATH, test "$enable_math" = "yes")
