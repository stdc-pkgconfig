
inherit eutils autotools

DESCRIPTION="pkg-config descriptors for A small VFS library"
HOMEPAGE="http://j.metux.de/index.php?option=com_content&task=view&id=57"
SRC_URI="http://releases.metux.de/${PN}/${PN}-${PV}.tar.bz2"

LICENSE="LGPL"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sparc x86"
IUSE=""

DEPEND=""

src_unpack() {
	unpack "${A}" || die
	cd "${S}" || die
	eautoreconf || die
}

src_compile() {
	econf 	\
	    --enable-pthread 	\
	    --enable-math		|| die
	    
	emake
}
	
src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog NEWS README
}
